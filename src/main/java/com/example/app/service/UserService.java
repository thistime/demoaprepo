package com.example.app.service;

public interface UserService {
    public void registerUser(String name, String surname,String email, String tel, String address);
}
