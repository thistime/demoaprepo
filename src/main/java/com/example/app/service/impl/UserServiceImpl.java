package com.example.app.service.impl;

import com.example.app.dao.UserDao;
import com.example.app.model.User;
import com.example.app.service.UserService;

public class UserServiceImpl implements UserService {

    UserDao userDao;

    public UserServiceImpl() {
    }

    @Override
    public void registerUser(String name, String surname,String email, String tel, String address) {
        User user = new User(name, surname, email, tel, address);
        userDao.save(user);
    }
}
