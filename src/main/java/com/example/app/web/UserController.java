package com.example.app.web;

import com.example.app.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller("/user")
public class UserController {

    UserService userService;

    @PostMapping("")
    public void registerUser(@RequestParam String name,
                             @RequestParam String surname,
                             @RequestParam String email,
                             @RequestParam String tel,
                             @RequestParam String address) {
        userService.registerUser(name, surname, email, tel, address);
    }
}
